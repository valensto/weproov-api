package config

import (
	"context"
	"os"
	"strconv"
	"strings"
)

type ctxKey int

const (
	configContextKey ctxKey = iota
)

type Mode int16

const (
	Local Mode = iota
	Development
	Staging
	Production
)

func (r Mode) String() string {
	if r < Local || r > Production {
		return "development"
	}
	return [...]string{"local", "development", "staging", "production"}[r]
}

func StringToMode(str string) Mode {
	lower := strings.ToLower(str)

	switch {
	case lower == "local":
		return Local
	case lower == "staging":
		return Staging
	case lower == "production":
		return Production
	default:
		return Development
	}
}

type App struct {
	Host      string
	Scheme    string
	Domain    string
	Port      string
	WebAppURL string
	Mode      Mode
}

type DB struct {
	Host     string
	Port     int
	User     string
	Password string
	Database string
	SSL      bool
}

type Mailer struct {
	Host     string
	Port     string
	Email    string
	Username string
	PWD      string
}

type Configuration struct {
	App    App
	DB     DB
	Mailer Mailer
}

func Load(ctx context.Context) (context.Context, error) {
	a := App{
		Host:      os.Getenv("HOST"),
		Scheme:    os.Getenv("SCHEME"),
		Domain:    os.Getenv("HOST"),
		Port:      os.Getenv("PORT"),
		WebAppURL: os.Getenv("WEBAPP_URL"),
		Mode:      StringToMode(os.Getenv("MODE")),
	}

	m := Mailer{
		Host:     os.Getenv("MAILER_HOST"),
		Port:     os.Getenv("MAILER_PORT"),
		Email:    os.Getenv("MAILER_PUBLIC"),
		Username: os.Getenv("MAILER_USERNAME"),
		PWD:      os.Getenv("MAILER_SECRET"),
	}

	dbPort, err := strconv.Atoi(os.Getenv("DB_PORT"))
	if err != nil {
		return nil, err
	}
	db := DB{
		Host:     os.Getenv("DB_HOST"),
		Port:     dbPort,
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PWD"),
		Database: os.Getenv("DB_NAME"),
		SSL:      os.Getenv("DB_SSL") == "true",
	}

	conf := Configuration{
		App:    a,
		DB:     db,
		Mailer: m,
	}

	return setConfigToCtx(ctx, conf), nil
}

func setConfigToCtx(ctx context.Context, config Configuration) context.Context {
	ctx = context.WithValue(ctx, configContextKey, config)
	return ctx
}

func FromCtx(ctx context.Context) Configuration {
	config, ok := ctx.Value(configContextKey).(Configuration)
	if !ok {
		panic("enable to find config in context")
	}
	return config
}
