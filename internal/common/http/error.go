package http

import (
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
	"weproov.com/core/pkg/errors"
	"weproov.com/core/pkg/logs"
)

func InternalError(slug string, err error, r *http.Request) ErrorResponse {
	return httpRespondWithError(err, slug, r, "Internal server error", http.StatusInternalServerError)
}

func Unauthorised(slug string, err error, r *http.Request) ErrorResponse {
	return httpRespondWithError(err, slug, r, "Unauthorised", http.StatusUnauthorized)
}

func Forbidden(slug string, err error, r *http.Request) ErrorResponse {
	return httpRespondWithError(err, slug, r, "Forbidden", http.StatusForbidden)
}

func NotImplemented(slug string, err error, r *http.Request) ErrorResponse {
	return httpRespondWithError(err, slug, r, "Not implemented", http.StatusNotImplemented)
}

func BadRequest(slug string, err error, r *http.Request) ErrorResponse {
	return httpRespondWithError(err, slug, r, "Bad request", http.StatusBadRequest)
}

func NotFound(slug string, err error, r *http.Request) ErrorResponse {
	return httpRespondWithError(err, slug, r, "Bad request", http.StatusNotFound)
}

func (s *Server) RespondWithErr(w http.ResponseWriter, r *http.Request, err error) {
	if err == nil {
		err = errors.NewInternalError("unknown error", errors.ErrUnknownError)
	}

	var resp ErrorResponse
	slugError, ok := err.(errors.SlugError)
	if !ok {
		resp = InternalError("internal-server-error", err, r)
		return
	}

	switch slugError.ErrorType() {
	case errors.ErrorTypeAuthorization:
		resp = Unauthorised(slugError.Slug(), slugError, r)
		break
	case errors.ErrorTypeIncorrectInput:
		resp = BadRequest(slugError.Slug(), slugError, r)
		break
	case errors.ErrorTypeForbidden:
		resp = Forbidden(slugError.Slug(), slugError, r)
		break
	case errors.ErrorTypeNotFound:
		resp = NotFound(slugError.Slug(), slugError, r)
		break
	case errors.ErrorTypeNotImplemented:
		resp = NotImplemented(slugError.Slug(), slugError, r)
		break
	default:
		resp = InternalError(slugError.Slug(), slugError, r)
	}

	s.Respond(w, r, resp.HttpStatus, resp)
}

func httpRespondWithError(err error, slug string, r *http.Request, logMSg string, status int) ErrorResponse {
	respErr := ErrorResponse{
		Slug:       slug,
		HttpStatus: status,
	}
	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		respErr.ReqID = reqID
	}
	logs.GetLogEntry(r).WithError(err).WithField("error-slug", slug).WithField("req-id", respErr.ReqID).Warn(logMSg)
	return respErr
}

type ErrorResponse struct {
	Slug       string `json:"slug"`
	ReqID      string `json:"req_id"`
	HttpStatus int    `json:"-"`
}
