package http

import (
	"context"
	"github.com/golang-jwt/jwt"
	"github.com/golang-jwt/jwt/request"
	"net/http"
	"strings"
	"weproov.com/core/internal/common/types"
	_context "weproov.com/core/pkg/context"
	"weproov.com/core/pkg/errors"
)

func claimExists(claims map[string]interface{}, key string) bool {
	val, ok := claims[key]
	return ok && val != nil
}

func (s *Server) tokenFromHeader(r *http.Request) string {
	headerValue := r.Header.Get("Authorization")

	if len(headerValue) > 7 && strings.ToLower(headerValue[0:6]) == "bearer" {
		return headerValue[7:]
	}

	return ""
}

func (s *Server) authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		bearerToken := s.tokenFromHeader(r)
		if bearerToken == "" {
			next.ServeHTTP(w, r)
			return
		}

		token, err := s.AuthClient.VerifyIDToken(ctx, bearerToken)
		if err != nil {
			s.RespondWithErr(w, r, errors.NewAuthorizationError(err.Error(), errors.ErrUnableToVerifyToken))
			return
		}

		claims := token.Claims
		if !claimExists(claims, "role") || !claimExists(claims, "email") || !claimExists(claims, "internal_uuid") || !claimExists(claims, "name") {
			s.RespondWithErr(w, r, errors.NewAuthorizationError("invalid claims", errors.ErrInvalidClaims))
			return
		}

		role, err := types.StringToRole(claims["role"].(string))
		if err != nil {
			s.RespondWithErr(w, r, errors.NewAuthorizationError(err.Error(), errors.ErrInvalidRole))
			return
		}
		ctx = _context.SetUserToCtx(ctx, _context.User{
			UUID:        claims["internal_uuid"].(string),
			AccountUUID: types.AccountID(token.UID),
			Email:       claims["email"].(string),
			Role:        role,
			DisplayName: claims["name"].(string),
		})
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

func (s *Server) authMockMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bearerToken := s.tokenFromHeader(r)
		if bearerToken == "" {
			next.ServeHTTP(w, r)
			return
		}

		var claims jwt.MapClaims
		token, err := request.ParseFromRequest(
			r,
			request.AuthorizationHeaderExtractor,
			func(token *jwt.Token) (i interface{}, e error) {
				return []byte("mock-secret"), nil
			},
			request.WithClaims(&claims),
		)
		if err != nil {
			s.RespondWithErr(w, r, errors.NewAuthorizationError(err.Error(), errors.ErrEmptyBearerToken))
			return
		}

		if !token.Valid {
			s.RespondWithErr(w, r, errors.NewAuthorizationError("Invalid token", errors.ErrJWTInvalid))
			return
		}

		role, err := types.StringToRole(claims["role"].(string))
		if err != nil {
			s.RespondWithErr(w, r, errors.NewAuthorizationError(err.Error(), errors.ErrInvalidRole))
			return
		}
		ctx := _context.SetUserToCtx(r.Context(), _context.User{
			UUID:        claims["internal_uuid"].(string),
			AccountUUID: types.AccountID(claims["user_id"].(string)),
			Email:       claims["email"].(string),
			Role:        role,
			DisplayName: claims["name"].(string),
		})
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

func (s *Server) setAuthMiddleware() error {
	/*	if s.Conf.AuthMock {
			logrus.Info("Auth mock enabled")
			s.Router.Use(s.authMockMiddleware)
			return nil
		}

		authClient, err := auth.New()
		if err != nil {
			return err
		}

		s.AuthClient = authClient
		s.Router.Use(s.authMiddleware)*/
	return nil
}

func (s *Server) GetCurrentUser(ctx context.Context) (_context.User, error) {
	u, err := _context.UserFromCtx(ctx)
	if err != nil {
		return _context.User{}, errors.NewAuthorizationError(err.Error(), errors.NoUserInContextError)
	}

	return u, nil
}

func (s *Server) Is(ctx context.Context, roles ...types.Role) error {
	u, err := s.GetCurrentUser(ctx)
	if err != nil {
		return err
	}

	roles = append(roles, types.Support)
	if !u.Role.Is(roles...) {
		return errors.NewForbiddenError(err.Error(), errors.ErrNotAuthorized)
	}

	return nil
}

func (s *Server) IsAuth(ctx context.Context) error {
	_, err := s.GetCurrentUser(ctx)
	if err != nil {
		return err
	}

	return nil
}
