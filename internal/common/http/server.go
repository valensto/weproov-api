package http

import (
	"bytes"
	"context"
	"encoding/json"
	"firebase.google.com/go/auth"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/gorilla/schema"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"reflect"
	"strings"
	"time"
	"weproov.com/core/internal/common/config"
	errors2 "weproov.com/core/pkg/errors"
	"weproov.com/core/pkg/filter"
	"weproov.com/core/pkg/logs"
)

type Server struct {
	Router     *chi.Mux
	Conf       config.App
	AuthClient *auth.Client
	Decoder    *schema.Decoder
}

func NewServer(ctx context.Context) (*Server, error) {
	conf := config.FromCtx(ctx)

	decoder := schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)
	decoder.ZeroEmpty(true)

	decoder.RegisterConverter([]string{}, func(input string) reflect.Value {
		return reflect.ValueOf(strings.Split(input, ","))
	})

	s := &Server{
		Router:  chi.NewRouter(),
		Conf:    conf.App,
		Decoder: decoder,
	}
	if err := s.init(); err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Server) init() error {
	err := s.setCommonMiddlewares()
	if err != nil {
		return err
	}
	s.Router.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	s.Router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		s.RespondWithErr(w, r, errors2.NewNotImplementedError("route not implemented", errors2.ErrNotImplemented))
	})

	return nil
}

func (s *Server) Serve() error {
	return http.ListenAndServe(":"+s.Conf.Port, s.Router)
}

func (s *Server) setCommonMiddlewares() error {
	s.Router.Use(s.langMiddleware)
	s.Router.Use(logs.NewStructuredLogger(logrus.StandardLogger()))
	s.Router.Use(middleware.RequestID)
	s.Router.Use(middleware.RealIP)
	s.Router.Use(middleware.Recoverer)
	s.corsMiddleware()
	err := s.setAuthMiddleware()
	if err != nil {
		return err
	}

	return nil
}

func (s *Server) GetParam(r *http.Request, k string) string {
	return chi.URLParam(r, k)
}

func (s *Server) Respond(w http.ResponseWriter, _ *http.Request, status int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)

	if data == nil {
		return
	}

	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		log.Printf("cannot format json. err=%v\n", err)
	}
}

func (s *Server) DownloadFile(w http.ResponseWriter, r *http.Request, data []byte, filename string) {
	attachment := fmt.Sprintf("attachment; filename=%v", filename)
	w.Header().Set("Content-Disposition", attachment)
	http.ServeContent(w, r, filename, time.Now(), bytes.NewReader(data))
}

func (s *Server) RespondWithContentLocation(w http.ResponseWriter, r *http.Request, status int, uri string) {
	w.Header().Add("content-location", uri)
	s.Respond(w, r, status, nil)
}

func (s *Server) ResponseWithPaginate(w http.ResponseWriter, r *http.Request, meta filter.MetaPagination, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusPartialContent)

	if data == nil {
		return
	}

	resp := filter.DataPagination{
		Meta:  meta,
		Links: meta.GenerateLinks(r.URL.RequestURI()),
		Data:  data,
	}

	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		log.Printf("cannot format json. err=%v\n", err)
	}
}

func (s *Server) ParseQueryParams(w http.ResponseWriter, r *http.Request) filter.CommonFilters {
	f, err := filter.DecodeQueryToCommonFilters(r.URL.Query())
	if err != nil {
		s.RespondWithErr(w, r, err)
	}

	return f
}

func (s *Server) Decode(_ http.ResponseWriter, r *http.Request, v interface{}) error {
	if r.ContentLength == 0 {
		return nil
	}

	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		return errors2.NewIncorrectInputError(err.Error(), errors2.ErrPayloadNotValid)
	}

	return nil
}