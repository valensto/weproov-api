package http

import (
	"github.com/go-chi/cors"
	"net/http"
	"os"
	"strings"
	"weproov.com/core/pkg/context"
	"weproov.com/core/pkg/locales"
)

func (s *Server) getIOSLang(r *http.Request) locales.Locale {
	header := r.Header
	languagesRequest := header.Get("Accept-Language")
	languages := strings.Split(languagesRequest, ",")
	for _, language := range languages {
		languageWithoutQuality := strings.Split(language, ";")[0]
		languageDetected := strings.Split(languageWithoutQuality, "-")[0]
		if iso, ok := locales.StringToLocale(languageDetected); ok {
			return iso
		}
	}
	return locales.FR
}

func (s *Server) langMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		iso := s.getIOSLang(r)

		ctx = context.SetLocal(ctx, iso)
		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (s *Server) corsMiddleware() {
	allowedOrigins := strings.Split(os.Getenv("CORS_ALLOWED_ORIGINS"), ";")
	if len(allowedOrigins) == 0 {
		return
	}

	corsMiddleware := cors.New(cors.Options{
		AllowedOrigins:   allowedOrigins,
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "Accept-Language"},
		ExposedHeaders:   []string{"Link", "Content-Location", "Content-Disposition"},
		AllowCredentials: true,
		MaxAge:           300,
	})
	s.Router.Use(corsMiddleware.Handler)
}
