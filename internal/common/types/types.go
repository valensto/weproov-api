package types

import (
	"github.com/go-playground/validator"
	"strings"
	"time"
	errors2 "weproov.com/core/pkg/errors"
)

type (
	AccountID string
	Email     string
	Role      uint8
)

type Timestamps struct {
	CreatedAt time.Time
	UpdatedAt time.Time
}

var validate = validator.New()

func (id AccountID) ToString() string {
	return string(id)
}

func (email Email) ToString() string {
	return string(email)
}

func (email Email) IsValid() bool {
	if err := validate.Var(email, "required,email"); err != nil {
		return false
	}

	return true
}

const (
	Pilot Role = iota
	Speaker
	Administrator
	Admin
	Support
	Unknown
)

func StringToRole(r string) (Role, error) {
	switch strings.ToLower(r) {
	case "pilot":
		return Pilot, nil
	case "speaker":
		return Speaker, nil
	case "administrator":
		return Administrator, nil
	case "admin":
		return Admin, nil
	case "support":
		return Support, nil
	default:
		return Unknown, errors2.NewIncorrectInputError("invalid role", errors2.ErrInvalidRole)
	}
}

func MustStringToRole(r string) Role {
	role, _ := StringToRole(r)
	return role
}

func (r Role) Is(roles ...Role) bool {
	if len(roles) == 0 {
		return true
	}
	for _, role := range roles {
		if r == role {
			return true
		}
	}
	return false
}

func (r Role) ToString() string {
	if r < Pilot || r > Support {
		return "unknown"
	}
	return [...]string{"pilot", "speaker", "administrator", "admin", "support", "unknown"}[r]
}
