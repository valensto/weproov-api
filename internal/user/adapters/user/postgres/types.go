package postgres

import (
	"fmt"
	"time"
	"weproov.com/core/internal/user/app/query"
	"weproov.com/core/internal/user/domain/user"
)

type User struct {
	ID               string    `db:"id"`
	Firstname        string    `db:"firstname"`
	Lastname         string    `db:"lastname"`
	Email            string    `db:"email"`
	CreationDate     time.Time `db:"creationdate"`
	IsServiceAccount bool      `db:"isserviceaccount"`
}

func ToEntity(u User) (*user.User, error) {
	return user.MarshalFromDatabase(user.ID(u.ID), u.Firstname, u.Lastname, u.Email, u.CreationDate, u.IsServiceAccount)
}

func ToQuery(u User) query.User {
	fmt.Println(u.CreationDate)
	return query.User{
		ID:               user.ID(u.ID),
		Firstname:        u.Firstname,
		Lastname:         u.Lastname,
		Email:            user.Email(u.Email),
		CreatedAt:        u.CreationDate,
		IsServiceAccount: u.IsServiceAccount,
	}
}

func ToQueries(users []User) []query.User {
	var queries []query.User

	for _, u := range users {
		queries = append(queries, ToQuery(u))
	}

	return queries
}
