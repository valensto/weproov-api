package postgres

import (
	"context"
	"database/sql"
	_ "github.com/lib/pq"
	"weproov.com/core/internal/user/app/query"
	"weproov.com/core/internal/user/domain/user"
)

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) (*Repository, error) {
	return &Repository{db: db}, nil
}

func (r Repository) FindById(_ context.Context, id user.ID) (*user.User, error) {
	var u User
	err := r.db.QueryRow("SELECT * FROM Users WHERE id = $1", id).Scan(&u.ID, &u.Firstname, &u.Lastname, &u.Email, &u.CreationDate, &u.IsServiceAccount)
	if err != nil {
		return nil, err
	}

	return ToEntity(u)
}

func (r Repository) ReadAll(_ context.Context) ([]query.User, error) {
	rows, err := r.db.Query("SELECT * FROM Users ORDER BY creationdate DESC")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []User
	for rows.Next() {
		var u User
		if err := rows.Scan(&u.ID, &u.Firstname, &u.Lastname, &u.Email, &u.CreationDate, &u.IsServiceAccount); err != nil {
			return nil, err
		}
		users = append(users, u)
	}

	return ToQueries(users), nil
}

func (r Repository) ReadByID(_ context.Context, id user.ID) (query.User, error) {
	var u User
	err := r.db.QueryRow("SELECT * FROM Users WHERE id = $1", id).Scan(&u.ID, &u.Firstname, &u.Lastname, &u.Email, &u.CreationDate, &u.IsServiceAccount)
	if err != nil {
		return query.User{}, err
	}

	return ToQuery(u), nil
}

func (r Repository) Store(_ context.Context, user *user.User) error {
	_, err := r.db.Exec("INSERT INTO Users (firstname, lastname, email, creationdate, isserviceaccount) VALUES ($1, $2, $3, $4, $5)", user.Firstname(), user.Lastname(), user.Email().ToString(), user.CreatedAt(), user.IsServiceAccount())
	if err != nil {
		return nil
	}

	return nil
}

func (r Repository) Update(_ context.Context, user *user.User) error {
	_, err := r.db.Exec("UPDATE Users SET firstname = $1, lastname = $2, email = $3, creationdate = $4, isserviceaccount = $5 WHERE id = $6", user.Firstname(), user.Lastname(), user.Email().ToString(), user.CreatedAt(), user.IsServiceAccount(), user.Id().ToString())
	if err != nil {
		return err
	}

	return nil
}

func (r Repository) Delete(ctx context.Context, id user.ID) error {
	_, err := r.db.Exec("DELETE FROM Users WHERE id = $1", id)
	if err != nil {
		return err
	}
	
	return nil
}
