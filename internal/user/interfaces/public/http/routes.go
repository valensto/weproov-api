package http

import (
	"github.com/go-chi/chi/v5"
)

func (s Service) InitRoutes() {
	s.server.Router.Route("/v1/users", func(r chi.Router) {
		r.Get("/", s.users())
		r.Get("/{id}", s.userByID())
		r.Post("/", s.createUser())
		r.Put("/{id}/names", s.updateUserNames())
		r.Put("/{id}/email", s.updateUserEmail())
		r.Delete("/{id}", s.deleteUser())
	})
}
