package http

import (
	"weproov.com/core/internal/common/http"
	"weproov.com/core/internal/user/app"
)

type Service struct {
	server *http.Server
	app    app.Application
}

func NewService(s *http.Server, app app.Application) Service {
	return Service{
		server: s,
		app:    app,
	}
}
