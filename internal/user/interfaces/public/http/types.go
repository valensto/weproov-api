package http

import (
	"time"
	"weproov.com/core/internal/user/app/query"
)

type User struct {
	ID               string    `json:"id,omitempty"`
	Firstname        string    `json:"firstname"`
	Lastname         string    `json:"lastname"`
	Email            string    `json:"email"`
	CreatedAt        time.Time `json:"created_at,omitempty"`
	IsServiceAccount bool      `json:"isServiceAccount"`
}

type Names struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

type Email struct {
	Email string `json:"email"`
}

func queryUserToJSON(u query.User) User {
	return User{
		ID:               u.ID.ToString(),
		Firstname:        u.Firstname,
		Lastname:         u.Lastname,
		Email:            u.Email.ToString(),
		CreatedAt:        u.CreatedAt,
		IsServiceAccount: u.IsServiceAccount,
	}
}

func queriesUsersToJSON(users []query.User) []User {
	var jsonUsers []User

	for _, u := range users {
		jsonUsers = append(jsonUsers, queryUserToJSON(u))
	}

	return jsonUsers
}
