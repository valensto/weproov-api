package http

import (
	"net/http"
	"weproov.com/core/internal/user/app/command"
	"weproov.com/core/internal/user/domain/user"
)

func (s Service) createUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req User
		err := s.server.Decode(w, r, &req)
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		err = s.app.Commands.CreateUser(r.Context(), command.NewUser{
			Firstname:        req.Firstname,
			Lastname:         req.Lastname,
			Email:            user.Email(req.Email),
			IsServiceAccount: req.IsServiceAccount,
		})
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		s.server.Respond(w, r, http.StatusCreated, nil)
	}
}

func (s Service) updateUserNames() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		idParam := s.server.GetParam(r, "id")

		var req Names
		err := s.server.Decode(w, r, &req)
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		err = s.app.Commands.UpdateName(r.Context(), command.UpdateName{
			ID:        user.ID(idParam),
			Firstname: req.Firstname,
			Lastname:  req.Lastname,
		})
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		s.server.Respond(w, r, http.StatusOK, nil)
	}
}

func (s Service) updateUserEmail() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		idParam := s.server.GetParam(r, "id")

		var req Email
		err := s.server.Decode(w, r, &req)
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		err = s.app.Commands.UpdateEmail(r.Context(), command.UpdateEmail{
			ID:    user.ID(idParam),
			Email: user.Email(req.Email),
		})
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		s.server.Respond(w, r, http.StatusOK, nil)
	}
}

func (s Service) userByID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		idParam := s.server.GetParam(r, "id")

		u, err := s.app.Queries.UserByID(r.Context(), user.ID(idParam))
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		s.server.Respond(w, r, http.StatusOK, queryUserToJSON(u))
	}
}

func (s Service) users() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		users, err := s.app.Queries.ReadAll(r.Context())
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		s.server.Respond(w, r, http.StatusOK, queriesUsersToJSON(users))
	}
}

func (s Service) deleteUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		idParam := s.server.GetParam(r, "id")

		err := s.app.Commands.DeleteUser(r.Context(), user.ID(idParam))
		if err != nil {
			s.server.RespondWithErr(w, r, err)
			return
		}

		s.server.Respond(w, r, http.StatusOK, nil)
	}
}
