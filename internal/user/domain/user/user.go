package user

import (
	"time"
	"weproov.com/core/pkg/errors"
)

type User struct {
	id               ID
	firstname        string
	lastname         string
	email            Email
	createdAt        time.Time
	isServiceAccount bool
}

func NewUser(firstname string, lastname string, email Email, isServiceAccount bool) (*User, error) {
	if firstname == "" {
		return nil, errors.NewIncorrectInputError("firstname is empty", errors.ErrEmptyFirstname)
	}

	if lastname == "" {
		return nil, errors.NewIncorrectInputError("lastname is empty", errors.ErrEmptyLastname)
	}

	if err := email.Validate(); err != nil {
		return nil, err
	}

	return &User{firstname: firstname, lastname: lastname, email: email, createdAt: time.Now(), isServiceAccount: isServiceAccount}, nil
}

func MarshalFromDatabase(id ID, firstname string, lastname string, email string, createdAt time.Time, isServiceAccount bool) (*User, error) {
	u, err := NewUser(firstname, lastname, Email(email), isServiceAccount)
	if err != nil {
		return nil, err
	}

	u.id = id
	u.createdAt = createdAt

	return u, nil
}

func (u *User) Id() ID {
	return u.id
}

func (u *User) SetId(id ID) {
	u.id = id
}

func (u *User) Firstname() string {
	return u.firstname
}

func (u *User) SetFirstname(firstname string) {
	u.firstname = firstname
}

func (u *User) Lastname() string {
	return u.lastname
}

func (u *User) SetLastname(lastname string) {
	u.lastname = lastname
}

func (u *User) Email() Email {
	return u.email
}

func (u *User) SetEmail(email Email) error {
	if err := email.Validate(); err != nil {
		return err
	}

	u.email = email
	return nil
}

func (u *User) CreatedAt() time.Time {
	return u.createdAt
}

func (u *User) SetCreatedAt(createdAt time.Time) {
	u.createdAt = createdAt
}

func (u *User) IsServiceAccount() bool {
	return u.isServiceAccount
}

func (u *User) SetIsServiceAccount(isServiceAccount bool) {
	u.isServiceAccount = isServiceAccount
}
