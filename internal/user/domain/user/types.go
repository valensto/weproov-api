package user

import (
	"github.com/go-playground/validator/v10"
	"weproov.com/core/pkg/errors"
)

type (
	ID    string
	Email string
)

var validate = validator.New()

func (id ID) ToString() string {
	return string(id)
}

func (email Email) ToString() string {
	return string(email)
}

func (email Email) Validate() error {
	if err := validate.Var(email, "required,email"); err != nil {
		return errors.NewIncorrectInputError("email is invalid", errors.ErrInvalidEmail)
	}

	return nil
}
