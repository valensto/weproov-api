package user

import "context"

type Repository interface {
	FindById(ctx context.Context, id ID) (*User, error)
	Store(ctx context.Context, user *User) error
	Update(ctx context.Context, user *User) error
	Delete(ctx context.Context, id ID) error
}
