package query

import (
	"time"
	"weproov.com/core/internal/user/domain/user"
)

type User struct {
	ID               user.ID
	Firstname        string
	Lastname         string
	Email            user.Email
	CreatedAt        time.Time
	IsServiceAccount bool
}
