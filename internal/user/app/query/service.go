package query

import (
	"context"
	"weproov.com/core/internal/user/domain/user"
)

type (
	ReadModel interface {
		ReadByID(ctx context.Context, id user.ID) (User, error)
		ReadAll(_ context.Context) ([]User, error)
	}
)

type Service struct {
	readModel ReadModel
}

func NewService(readModel ReadModel) Service {
	return Service{
		readModel: readModel,
	}
}

func (s Service) UserByID(ctx context.Context, id user.ID) (User, error) {
	return s.readModel.ReadByID(ctx, id)
}

func (s Service) ReadAll(ctx context.Context) ([]User, error) {
	return s.readModel.ReadAll(ctx)
}
