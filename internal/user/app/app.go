package app

import (
	"weproov.com/core/internal/user/app/command"
	"weproov.com/core/internal/user/app/query"
)

type Application struct {
	Commands command.Service
	Queries  query.Service
}
