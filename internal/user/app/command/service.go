package command

import "weproov.com/core/internal/user/domain/user"

type Service struct {
	repository user.Repository
}

func NewService(repository user.Repository) Service {
	return Service{
		repository: repository,
	}
}
