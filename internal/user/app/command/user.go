package command

import (
	"context"
	"weproov.com/core/internal/user/domain/user"
)

func (s Service) CreateUser(ctx context.Context, cmd NewUser) error {
	u, err := user.NewUser(cmd.Firstname, cmd.Lastname, cmd.Email, cmd.IsServiceAccount)
	if err != nil {
		return err
	}

	return s.repository.Store(ctx, u)
}

func (s Service) UpdateName(ctx context.Context, cmd UpdateName) error {
	u, err := s.repository.FindById(ctx, cmd.ID)
	if err != nil {
		return err
	}

	u.SetFirstname(cmd.Firstname)
	u.SetLastname(cmd.Lastname)

	return s.repository.Update(ctx, u)
}

func (s Service) UpdateEmail(ctx context.Context, cmd UpdateEmail) error {
	u, err := s.repository.FindById(ctx, cmd.ID)
	if err != nil {
		return err
	}

	if err := u.SetEmail(cmd.Email); err != nil {
		return err
	}

	return s.repository.Update(ctx, u)
}

func (s Service) DeleteUser(ctx context.Context, id user.ID) error {
	return s.repository.Delete(ctx, id)
}
