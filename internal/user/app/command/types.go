package command

import "weproov.com/core/internal/user/domain/user"

type NewUser struct {
	Firstname        string
	Lastname         string
	Email            user.Email
	IsServiceAccount bool
}

type UpdateName struct {
	ID        user.ID
	Firstname string
	Lastname  string
}

type UpdateEmail struct {
	ID    user.ID
	Email user.Email
}
