package user

import (
	"context"
	commonHttp "weproov.com/core/internal/common/http"
	candidate "weproov.com/core/internal/user/adapters/user/postgres"
	"weproov.com/core/internal/user/app"
	"weproov.com/core/internal/user/app/command"
	"weproov.com/core/internal/user/app/query"
	"weproov.com/core/internal/user/interfaces/public/http"
	"weproov.com/core/pkg/store/postgres"
)

func New(_ context.Context, server *commonHttp.Server, store *postgres.Store) error {
	repository, err := candidate.NewRepository(store.DB)
	if err != nil {
		return err
	}

	application := app.Application{
		Commands: command.NewService(repository),
		Queries:  query.NewService(repository),
	}

	http.NewService(server, application).InitRoutes()
	return nil
}
