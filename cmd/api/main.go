package main

import (
	"context"
	"log"
	"os"
	"weproov.com/core/internal/common/config"
	"weproov.com/core/internal/common/http"
	"weproov.com/core/internal/user"
	"weproov.com/core/pkg/logs"
	"weproov.com/core/pkg/store/postgres"
)

func main() {
	logs.Init()

	if err := run(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func run() error {
	ctx := context.Background()

	ctx, err := config.Load(ctx)
	if err != nil {
		return err
	}

	conf := config.FromCtx(ctx)
	logs.Banner(conf.App.Host)

	store, err := postgres.New(ctx)
	if err != nil {
		return err
	}

	server, err := http.NewServer(ctx)
	if err != nil {
		return err
	}

	err = user.New(ctx, server, store)
	if err != nil {
		return err
	}

	return server.Serve()
}
