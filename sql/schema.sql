CREATE TABLE IF NOT EXISTS Users (
    id    SERIAL PRIMARY KEY,
    firstname VARCHAR(50) NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    creationdate TIMESTAMP NOT NULL,
    isserviceaccount BOOLEAN NOT NULL DEFAULT FALSE
);