include .env
export

NETWORKS="$(shell docker network ls)"
VOLUMES="$(shell docker volume ls)"
SUCCESS=[ done "\xE2\x9C\x94" ]

# default arguments
user ?= root
service ?= weproov-api

.PHONY: all
## Run all services in docker
all: --postgres-network
	@echo [ starting containers... ]
	docker-compose up weproov-postgres weproov-api

.PHONY: pgadmin
## Run pgadmin in docker
pgadmin:
	@echo [ starting pgadmin container... ]
	docker-compose up weproov-pgadmin -d
	@echo $(SUCCESS) pgadmin running on port http://127.0.0.1:$(DB_ADMIN_PORT)

.PHONY: stop-pgadmin
## Stop pgadmin in docker
stop-pgadmin:
	@echo [ stopping pgadmin container... ]
	docker-compose stop weproov-pgadmin
	@echo $(SUCCESS)

.PHONY: pg-schemas
## Execute database schemas
pg-schemas:
	@echo [ creating postgres schemas... ]
	docker exec -it weproov-postgres psql -U $(DB_USER) -d $(DB_NAME) -f /sql/schema.sql
	@echo $(SUCCESS)

.PHONE: tidy
## Cleaning and downloading all go dependencies in docker
tidy:
	@echo [ cleaning up unused $(service) dependencies... ]
	@docker exec weproov-api go mod tidy

.PHONE: exec
## Executing command in docker container
exec:
	@echo [ executing $(cmd) in $(service) ]
	docker-compose exec -u $(user) $(service) $(cmd)
	@echo $(SUCCESS)

.PHONY: test
## Run tests outside docker
test:
	@echo "\n.... Running tests for ${PROJECT_NAME} ...."
	@reflex -c ./reflex.conf

.PHONY: run-test
run-test:
	@./scripts/test.sh ./internal/common .env
	@./scripts/test.sh ./internal/candidate .env
	@./scripts/test.sh ./pkg/filter .env

.PHONY: down
## Stop and remove all containers
down:
	@echo [ teardown all containers... ]
	docker-compose down
	@echo $(SUCCESS)

.PHONY: clear
## Stop and remove all containers and volumes
clear: --remove-postgres-volume down
	@echo $(SUCCESS)

.PHONY: --remove-postgres-volume
--remove-postgres-volume:
ifneq (,$(findstring postgres-db,$(VOLUMES)))
	@echo [ creating postgres volume... ]
	docker volume rm postgres-weproov-vol
	@echo $(SUCCESS)
endif

.PHONY: --postgres-network
--postgres-network:
ifeq (,$(findstring postgres-weproov-net,$(NETWORKS)))
	@echo [ creating postgres network... ]
	docker network create postgres-weproov-net
	@echo $(SUCCESS)
endif

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

TARGET_MAX_CHAR_NUM=20
## Show help
help:
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)