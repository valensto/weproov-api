# WEPROOV API

### routes

- POST /v1/users - create a new user
- GET /v1/users - get all users
- GET /v1/users/:id - get a user by id 
- PUT /v1/users/:id/names - update a user's name
- PUT /v1/users/:id/email - update a user's email
- DELETE /v1/users/:id - delete a user by id

Pour lancer l'application run `make`
Désolé j'ai eu un soucis avec votre base de donnée, je n'ai pas pu la tester.
J'ai donc fait une base de donnée en local pour tester l'application, elle se lance dans docker.

Je n'ai pas eu le temps de mettre des variables d'environnement pour le front donc s'il vous plais gardez le port 4000 pour l'api :)

Au plaisir d'echanger avec vous.