FROM --platform=linux/amd64 golang:1.19-alpine as base
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates && \
apk add build-base && \
go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@v1.9.1 && \
go install github.com/cespare/reflex@latest
WORKDIR /api
COPY go.* ./
RUN go mod download && go env

FROM base as dev
RUN go install github.com/githubnemo/CompileDaemon@latest
EXPOSE 4000
ENTRYPOINT CompileDaemon --build="go build -o bin/weproov/api ./cmd/api" --command=./bin/weproov/api

FROM base as build-stage
COPY . .
RUN CGO_UNABLED=0 GOOS=linux go build -ldflags "-s -w" -o bin/weproov/api ./cmd/api

FROM alpine:latest as prod
COPY --from=base /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
WORKDIR /api
COPY --from=build-stage /api/bin/weproov/api api
EXPOSE 4000
CMD ["./api"]
