package templates

import "embed"

//go:embed pdf/*.gohtml email/*.gohtml
var Templates embed.FS

// this package is used to embed the templates in the binary.
// it is used by the http server to serve the templates or smtp server to send template emails.
// the templates are embedded in the binary to avoid having to copy them to the docker image.
