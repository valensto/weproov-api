package password

import (
	"math/rand"
	"strings"
	"time"
	"unicode"
	"weproov.com/core/pkg/errors"
)

const (
	lowerCharSet   = "abcdefghijklmnopqrstuvwxyz"
	upperCharSet   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	specialCharSet = "!@#$%&*"
	numberSet      = "0123456789"
	allCharSet     = lowerCharSet + upperCharSet + specialCharSet + numberSet
)

func GeneratePassword(passwordLength, minSpecialChar, minNum, minUpperCase int) string {
	var password strings.Builder
	rand.Seed(time.Now().Unix())

	for i := 0; i < minSpecialChar; i++ {
		random := rand.Intn(len(specialCharSet))
		password.WriteString(string(specialCharSet[random]))
	}

	for i := 0; i < minNum; i++ {
		random := rand.Intn(len(numberSet))
		password.WriteString(string(numberSet[random]))
	}

	for i := 0; i < minUpperCase; i++ {
		random := rand.Intn(len(upperCharSet))
		password.WriteString(string(upperCharSet[random]))
	}

	remainingLength := passwordLength - minSpecialChar - minNum - minUpperCase
	for i := 0; i < remainingLength; i++ {
		random := rand.Intn(len(allCharSet))
		password.WriteString(string(allCharSet[random]))
	}
	inRune := []rune(password.String())
	rand.Shuffle(len(inRune), func(i, j int) {
		inRune[i], inRune[j] = inRune[j], inRune[i]
	})
	return string(inRune)
}

func IsStrongEnough(pwd string) error {
	length, number, upper, special := verify(pwd, 7)
	if len(pwd) == 0 {
		return errors.NewIncorrectInputError("password is empty", errors.ErrEmptyPassword)
	}
	if !length {
		return errors.NewIncorrectInputError("password is too short", errors.ErrPasswordTooShort)
	}
	if !number {
		return errors.NewIncorrectInputError("password must contain at least one number", errors.ErrPasswordNoNumbers)
	}
	if !upper {
		return errors.NewIncorrectInputError("password must contain at least one uppercase letter", errors.ErrPasswordNoUppercase)
	}
	if !special {
		return errors.NewIncorrectInputError("password must contain at least one special character", errors.ErrPasswordNoSpecials)
	}
	return nil
}

func verify(pwd string, minLength int) (length, number, upper, special bool) {
	number = false
	upper = false
	special = false
	for _, c := range pwd {
		switch {
		case unicode.IsNumber(c):
			number = true
		case unicode.IsUpper(c):
			upper = true
		case unicode.IsPunct(c) || unicode.IsSymbol(c):
			special = true
		case unicode.IsLetter(c) || c == ' ':
		}
	}
	length = len(pwd) >= minLength
	return length, number, upper, special
}
