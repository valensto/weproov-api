package logs

import (
	"fmt"
	"github.com/fatih/color"
	"os"
	"os/exec"
	"time"
)

func Banner(baseURL string) {
	b := `
██╗    ██╗███████╗██████╗ ██████╗  ██████╗  ██████╗ ██╗   ██╗
██║    ██║██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔═══██╗██║   ██║
██║ █╗ ██║█████╗  ██████╔╝██████╔╝██║   ██║██║   ██║██║   ██║
██║███╗██║██╔══╝  ██╔═══╝ ██╔══██╗██║   ██║██║   ██║╚██╗ ██╔╝
╚███╔███╔╝███████╗██║     ██║  ██║╚██████╔╝╚██████╔╝ ╚████╔╝ 
 ╚══╝╚══╝ ╚══════╝╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝   ╚═══╝
`
	url := fmt.Sprintf("base url: %v", baseURL)

	t := time.Now()
	zoneName, _ := t.Zone()
	timezone := fmt.Sprintf("Current timezone: %s", zoneName)
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	_ = c.Run()
	color.Cyan(b)
	color.Cyan(url)
	color.Cyan(timezone)
	fmt.Println("-----------------------------------------------------------------------")
}
