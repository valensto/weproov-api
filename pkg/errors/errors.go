package errors

type ErrorType struct {
	t string
}

var (
	ErrorTypeUnknown        = ErrorType{"unknown"}
	ErrorTypeAuthorization  = ErrorType{"authorization"}
	ErrorTypeForbidden      = ErrorType{"forbidden"}
	ErrorTypeIncorrectInput = ErrorType{"incorrect-input"}
	ErrorTypeNotFound       = ErrorType{"not-found"}
	ErrorTypeDuplicate      = ErrorType{"duplicate"}
	ErrorTypeNotImplemented = ErrorType{"not-implemented"}
)

type SlugError struct {
	error     string
	slug      Slug
	errorType ErrorType
}

func (s SlugError) Error() string {
	return s.error
}

func (s SlugError) Slug() string {
	return string(s.slug)
}

func (s SlugError) ErrorType() ErrorType {
	return s.errorType
}

func NewAuthorizationError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeAuthorization,
	}
}

func NewForbiddenError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeForbidden,
	}
}

func NewIncorrectInputError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeIncorrectInput,
	}
}

func NewDuplicateError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeDuplicate,
	}
}

func NewNotImplementedError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeNotImplemented,
	}
}

func NewNotFoundError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeNotFound,
	}
}

func NewInternalError(error string, slug Slug) SlugError {
	return SlugError{
		error:     error,
		slug:      slug,
		errorType: ErrorTypeUnknown,
	}
}
