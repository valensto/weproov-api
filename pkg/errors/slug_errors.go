package errors

type Slug string

func (s Slug) ToString() string {
	return string(s)
}

const (
	ErrUnknownError        Slug = "unknown-error"
	ErrNotFound            Slug = "NOT_FOUND"
	ErrFailedToStoreEntry  Slug = "FAILED_TO_STORE_ENTRY"
	ErrFailedToUpdateEntry Slug = "FAILED_TO_UPDATE_ENTRY"
	ErrFailedToGetEntry    Slug = "FAILED_TO_GET_ENTRY"
	ErrCannotMarshall      Slug = "CANNOT_MARSHALL"
	ErrCannotUnmarshall    Slug = "CANNOT_UNMARSHALL"
	ErrNotImplemented      Slug = "NOT_IMPLEMENTED"

	ErrBadUUIDFormat  Slug = "BAD_UUID_FORMAT"
	ErrEmptyUUID      Slug = "UUID_IS_EMPTY"
	ErrEmptySlug      Slug = "SLUG_IS_EMPTY"
	ErrEmptyAccountID Slug = "ACCOUNT_ID_IS_EMPTY"
	ErrInvalidPhone   Slug = "PHONE_IS_INVALID"
	ErrParsingFilters Slug = "UNABLE_TO_PARSE_QUERY_FILTERS"
	ErrDateFormat     Slug = "DATE_BAD_FORMAT"

	ErrZeroTime       Slug = "ZERO_TIME"
	ErrEmptyFirstname Slug = "EMPTY_FIRSTNAME"
	ErrEmptyLastname  Slug = "EMPTY_LASTNAME"
	ErrInvalidEmail   Slug = "INVALID_EMAIL"

	ErrInvalidRole Slug = "ROLE_IS_INVALID"

	ErrIncorrectCredential  Slug = "INCORRECT_CREDENTIAL"
	ErrEmptyPassword        Slug = "EMPTY_PASSWORD"
	ErrPasswordTooShort     Slug = "PASSWORD_TOO_SHORT"
	ErrPasswordNoNumbers    Slug = "PASSWORD_NO_NUMBERS"
	ErrPasswordNoUppercase  Slug = "PASSWORD_NO_UPPERCASE"
	ErrPasswordNoSpecials   Slug = "PASSWORD_NO_SPECIALS"
	ErrFailedToHashPassword Slug = "FAILED_TO_HASH_PASSWORD"

	ErrInvalidGhostKey     Slug = "invalid-ghost-key"
	NoUserInContextError   Slug = "NO_USER_IN_CONTEXT"
	ErrJWTInvalid          Slug = "JWT_INVALID"
	ErrEmptyBearerToken    Slug = "EMPTY_BEARER_TOKEN"
	ErrUnableToVerifyToken Slug = "UNABLE_TO_VERIFY_TOKEN"
	ErrNotAuthorized       Slug = "NOT_AUTHORIZED"
	ErrInvalidClaims       Slug = "INVALID_CLAIMS"

	ErrEmptyStartAt         Slug = "EMPTY_START_AT"
	ErrEndAtAfterStartAt    Slug = "END_AT_AFTER_START_AT"
	ErrBadDescriptionLength Slug = "BAD_DESCRIPTION_LENGTH"
	ErrEmptyAuthorUUID      Slug = "EMPTY_AUTHOR_UUID"

	ErrInvalidSkillLevel  Slug = "INVALID_SKILL_LEVEL"
	ErrEmptySkillName     Slug = "EMPTY_SKILL_NAME"
	ErrSkillNotFound      Slug = "SKILL_NOT_FOUND"
	ErrSkillAlreadyExists Slug = "SKILL_ALREADY_EXISTS"

	ErrLanguageAlreadyExists Slug = "LANGUAGE_ALREADY_EXISTS"
	ErrInvalidLanguageLevel  Slug = "INVALID_LANGUAGE_LEVEL"
	ErrEmptyLanguage         Slug = "EMPTY_LANGUAGE"
	ErrLanguageNotFound      Slug = "LANGUAGE_NOT_FOUND"

	ErrExperienceNotFound Slug = "EXPERIENCE_NOT_FOUND"
	ErrEmptyKind          Slug = "EMPTY_KIND"
	ErrEmptyCompany       Slug = "EMPTY_COMPANY"
	ErrEmptyLocation      Slug = "EMPTY_LOCATION"

	ErrEmptySchool Slug = "EMPTY_SCHOOL"
	ErrEmptyDegree Slug = "EMPTY_DEGREE"
	ErrEmptyDomain Slug = "EMPTY_DOMAIN"

	ErrEmptyFromExperience    Slug = "EMPTY_FROM_EXPERIENCE"
	ErrEmptyRefererLastname   Slug = "EMPTY_REFERER_LASTNAME"
	ErrEmptyRefererFirstname  Slug = "EMPTY_REFERER_FIRSTNAME"
	ErrBadRefererEmail        Slug = "BAD_REFERER_EMAIL"
	ErrRecommendationNotFound Slug = "RECOMMENDATION_NOT_FOUND"

	ErrEmptyCountry                     Slug = "EMPTY_COUNTRY"
	ErrEmptyCity                        Slug = "EMPTY_CITY"
	ErrInvalidLatitude                  Slug = "INVALID_LATITUDE"
	ErrInvalidLongitude                 Slug = "INVALID_LONGITUDE"
	ErrInvalidLocationToOfficeWorkplace Slug = "INVALID_LOCATION_TO_OFFICE_WORKPLACE"

	ErrPayloadNotValid Slug = "PAYLOAD_NOT_VALID"
)
