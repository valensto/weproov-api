package locales

import "strings"

type Locale int16

const (
	FR Locale = iota
	EN
)

func (r Locale) String() string {
	if r < FR || r > EN {
		return "en"
	}

	return [...]string{"fr", "en"}[r]
}

func StringToLocale(str string) (Locale, bool) {
	lower := strings.ToLower(str)

	switch {
	case lower == "fr-FR" || lower == "fr":
		return FR, true
	default:
		return EN, false
	}
}
