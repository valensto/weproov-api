package context

import (
	"context"
	"weproov.com/core/internal/common/types"
	errors2 "weproov.com/core/pkg/errors"
	"weproov.com/core/pkg/locales"
)

type ctxKey int

const (
	PublicPathsContextKey ctxKey = iota
	langContextKey
	userContextKey
)

type User struct {
	UUID        string
	AccountUUID types.AccountID
	Email       string
	Role        types.Role

	DisplayName string
}

func UserFromCtx(ctx context.Context) (User, error) {
	v := ctx.Value(userContextKey)
	if v == nil {
		return User{}, errors2.NewAuthorizationError("no user in context", errors2.NoUserInContextError)
	}

	u, ok := v.(User)
	if ok {
		return u, nil
	}

	return User{}, errors2.NewAuthorizationError("no user in context", errors2.NoUserInContextError)
}

func SetUserToCtx(ctx context.Context, u User) context.Context {
	ctx = context.WithValue(ctx, userContextKey, u)
	return ctx
}

func GetPublicPaths(ctx context.Context) []string {
	u, ok := ctx.Value(PublicPathsContextKey).([]string)
	if ok {
		return u
	}

	return nil
}

func GetLocal(ctx context.Context) locales.Locale {
	u, ok := ctx.Value(langContextKey).(locales.Locale)
	if ok {
		return u
	}
	return locales.EN
}

func SetLocal(ctx context.Context, iso locales.Locale) context.Context {
	ctx = context.WithValue(ctx, langContextKey, iso)

	return ctx
}
