package filter

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
)

type DataPagination struct {
	Meta  MetaPagination `json:"meta"`
	Links Links          `json:"links"`
	Data  interface{}    `json:"data"`
}

type Paginate struct {
	Limit int `schema:"limit"`
	Page  int `schema:"page"`
}

type MetaPagination struct {
	PerPages      int `json:"perPages"`
	TotalElements int `json:"totalElements"`
	TotalPages    int `json:"totalPages"`
	CurrentPage   int `json:"currentPage"`
}

type Links struct {
	First string `json:"first,omitempty"`
	Prev  string `json:"prev,omitempty"`
	Self  string `json:"self,omitempty"`
	Next  string `json:"next,omitempty"`
	Last  string `json:"last,omitempty"`
}

func newPaginate() *Paginate {
	return &Paginate{
		Limit: 20,
		Page:  1,
	}
}

func (p *Paginate) updateLimit(limit int) error {
	if limit <= 0 {
		return errors.New("limit must be positive number")
	}

	p.Limit = limit
	return nil
}

func (p *Paginate) updateOffset(page int) error {
	if page <= 0 {
		return errors.New("page must be positive number")
	}

	p.Page = page
	return nil
}

func ParsePagination(qp Paginate) (Paginate, error) {
	p := newPaginate()
	if qp.Limit > 0 {
		err := p.updateLimit(qp.Limit)
		if err != nil {
			return Paginate{}, err
		}
	}

	if qp.Page > 0 {
		err := p.updateOffset(qp.Page)
		if err != nil {
			return Paginate{}, err
		}
	}

	return *p, nil
}

func (mp MetaPagination) GenerateLinks(base string) Links {
	links := Links{}
	u, err := url.Parse(base)
	if err != nil {
		return links
	}

	query := u.Query()
	query.Del("page")
	query.Set("limit", strconv.Itoa(mp.PerPages))

	if mp.TotalElements <= 0 {
		return links
	}

	links.Last = fmt.Sprintf("?%v&page=%v", query.Encode(), mp.TotalPages)
	links.First = fmt.Sprintf("?%v&page=%v", query.Encode(), 1)
	links.Self = fmt.Sprintf("?%v&page=%v", query.Encode(), mp.CurrentPage)

	if mp.CurrentPage > 1 {
		links.Prev = fmt.Sprintf("?%v&page=%v", query.Encode(), mp.CurrentPage-1)
	}

	if mp.CurrentPage < mp.TotalPages {
		links.Next = fmt.Sprintf("?%v&page=%v", query.Encode(), mp.CurrentPage+1)
	}

	return links
}
