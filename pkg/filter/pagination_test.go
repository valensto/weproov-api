package filter_test

import (
	"testing"
	"weproov.com/core/pkg/filter"
)

func TestGetLinks(t *testing.T) {
	var tests = []struct {
		meta     filter.MetaPagination
		base     string
		expected map[string]string
	}{
		{filter.MetaPagination{
			PerPages:      5,
			CurrentPage:   3,
			TotalElements: 122,
			TotalPages:    25,
		}, "?term=aaa", map[string]string{
			"self":  "?limit=5&term=aaa&page=3",
			"first": "?limit=5&term=aaa&page=1",
			"prev":  "?limit=5&term=aaa&page=2",
			"next":  "?limit=5&term=aaa&page=4",
			"last":  "?limit=5&term=aaa&page=25",
		}},
	}

	for _, tt := range tests {
		links := tt.meta.GenerateLinks(tt.base)

		if links.Self != tt.expected["self"] {
			t.Errorf("GetLinks failed to self, expected: %v, got: %v", tt.expected["self"], links.Self)
		}

		if links.First != tt.expected["first"] {
			t.Errorf("GetLinks failed to first, expected: %v, got: %v", tt.expected["first"], links.First)
		}

		if _, ok := tt.expected["prev"]; ok {
			if links.Prev != tt.expected["prev"] {
				t.Errorf("GetLinks failed to prev, expected: %v, got: %v", tt.expected["prev"], links.Prev)
			}
		}

		if _, ok := tt.expected["next"]; ok {
			if links.Next != tt.expected["next"] {
				t.Errorf("GetLinks failed to next, expected: %v, got: %v", tt.expected["next"], links.Next)
			}
		}

		if links.Last != tt.expected["last"] {
			t.Errorf("GetLinks failed to last, expected: %v, got: %v", tt.expected["last"], links.Last)
		}
	}
}
