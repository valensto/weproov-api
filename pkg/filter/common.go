package filter

import (
	"net/url"
	"time"

	"github.com/gorilla/schema"
)

type Query map[string][]string

type Sortable struct {
	Start   time.Time `schema:"start"`
	End     time.Time `schema:"end"`
	SortBy  string    `schema:"sort_by"`
	OrderBy string    `schema:"order_by"`
}

type queries struct {
	Sortable
	Paginate
	Populate bool   `schema:"populate"`
	Search   string `schema:"search"`
}

type CommonFilters struct {
	Pagination Paginate
	Sortable   Sortable
	Populate   bool
	Search     string
}

func DecodeQueryToCommonFilters(values url.Values) (CommonFilters, error) {
	q := new(queries)
	decoder := schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)
	decoder.ZeroEmpty(true)
	err := decoder.Decode(q, values)
	if err != nil {
		return CommonFilters{}, err
	}

	paginate, err := ParsePagination(q.Paginate)

	if err != nil {
		return CommonFilters{}, err
	}

	sortable := Sortable{
		Start:   q.Start,
		End:     q.End,
		SortBy:  q.SortBy,
		OrderBy: q.OrderBy,
	}

	return CommonFilters{
		Pagination: paginate,
		Sortable:   sortable,
		Populate:   q.Populate,
		Search:     q.Search,
	}, nil
}
