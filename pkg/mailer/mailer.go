package mailer

import (
	"bytes"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gopkg.in/gomail.v2"
	"html/template"
	"io/ioutil"
	"net/smtp"
	"os"
	"strconv"
	"weproov.com/core/internal/common/config"
	"weproov.com/core/internal/common/types"
	"weproov.com/core/templates"
)

type Mailer struct {
	Email    string
	Username string
	PWD      string
	Host     string
	Port     string
}

func New(c config.Mailer) (Mailer, error) {
	if len(c.Email) == 0 {
		return Mailer{}, errors.New("mailer email cannot be empty please add to env variables")
	}

	if len(c.PWD) == 0 {
		return Mailer{}, errors.New("mailer password cannot be empty please add to env variables")
	}

	if len(c.Username) == 0 {
		return Mailer{}, errors.New("mailer username cannot be empty please add to env variables")
	}

	if len(c.Host) == 0 {
		return Mailer{}, errors.New("mailer host cannot be empty please add to env variables")
	}

	if len(c.Port) == 0 {
		return Mailer{}, errors.New("mailer port cannot be empty please add to env variables")
	}
	return Mailer{
		Email:    c.Email,
		Username: c.Username,
		PWD:      c.PWD,
		Host:     c.Host,
		Port:     c.Port,
	}, nil
}

func (m Mailer) NewMail(to []types.Email, subject string) *Mail {
	return &Mail{to: to, subject: subject}
}

func (m Mailer) Send(mail *Mail) {
	auth := smtp.PlainAuth("", m.Username, m.PWD, m.Host)
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := fmt.Sprintf("Subject: %v\n", mail.subject)
	msg := []byte(subject + mime + mail.body.String())

	err := smtp.SendMail(m.Host+":"+m.Port, auth, m.Email, mail.toToString(), msg)
	if err != nil {
		log.Warn(err.Error())
	}
}

func (m Mailer) SendWithPDF(mail *Mail, files map[string][]byte) {
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", m.Email)
	mailer.SetHeader("To", mail.toToString()...)
	mailer.SetHeader("Subject", mail.subject)
	mailer.SetBody("text/html", mail.body.String())

	for i, v := range files {
		err := ioutil.WriteFile(fmt.Sprintf("%v", i), v, 0644)
		if err != nil {
			log.Warn(err.Error())
		}
		mailer.Attach(fmt.Sprintf("%v", i))
	}

	port, err := strconv.Atoi(m.Port)
	if err != nil {
		log.Warn(err.Error())
	}

	d := gomail.NewDialer(m.Host, port, m.Username, m.PWD)
	if err := d.DialAndSend(mailer); err != nil {
		log.Warn(err)
	}

	for i, _ := range files {
		err := os.Remove(fmt.Sprintf("%v", i))
		if err != nil {
			log.Warn(err)
		}
	}
}

type Mail struct {
	to      []types.Email
	body    *bytes.Buffer
	subject string
}

func (m *Mail) toToString() []string {
	var to []string
	for _, v := range m.to {
		to = append(to, v.ToString())
	}
	return to
}

func (m *Mail) AddBody(b *bytes.Buffer) {
	m.body = b
}

func (m *Mail) ParseTemplate(templateFileName string, data interface{}) error {
	t, err := template.ParseFS(templates.Templates, templateFileName)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return err
	}
	m.AddBody(buf)
	return nil
}
