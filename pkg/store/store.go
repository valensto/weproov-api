package store

import (
	"github.com/google/uuid"
	"weproov.com/core/pkg/errors"
)

func GenerateUUID() uuid.UUID {
	return uuid.New()
}

func UUIDFromBytes(b []byte) (uuid.UUID, error) {
	fromBytes, err := uuid.FromBytes(b)
	if err != nil {
		return [16]byte{}, errors.NewIncorrectInputError("bad uuid format", errors.ErrBadUUIDFormat)
	}

	return fromBytes, nil
}
