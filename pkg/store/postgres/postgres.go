package postgres

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"weproov.com/core/internal/common/config"
)

type Store struct {
	Config config.DB
	DB     *sql.DB
}

func New(ctx context.Context) (*Store, error) {
	conf := config.FromCtx(ctx)

	connectionString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", conf.DB.Host, conf.DB.Port, conf.DB.User, conf.DB.Password, conf.DB.Database)
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return &Store{
		Config: conf.DB,
		DB:     db,
	}, nil
}

func (s *Store) Close() error {
	return s.DB.Close()
}
