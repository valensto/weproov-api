package slug

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/gosimple/slug"
)

func Make(s string) string {
	return slug.Make(s)
}

func MakeWithHash(s string, size int) string {
	return fmt.Sprintf("%s-%s", Make(s), randSeq(size))
}

func randSeq(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyz0123456789")
	b := make([]rune, n)
	rand.Seed(time.Now().Unix())
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
